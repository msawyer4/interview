#######################
#  Solution Function  #
#######################

#  New game plan:
## Need to iterate through mySet, find largest result of List.index(x)
## Delete up to that index
## flip it, repeat
## Then the leftovers should be -1 from the value to return.

def solution (List):
  mySet = set(List)
  max = 0
  for x in mySet:
    place = List.index(x)
    if (place > max):
      max = place
  x = 0
  while (x <= max):
    List.pop(0)
    x+=1
  List.reverse()
  try:
    max = 0
    for x in mySet:
      place = List.index(x)
      if (place > max):
        max = place
    x = 0
    while (x <= max):
      List.pop(0)
      x+=1
    return len(List)+1
  except ValueError:
    return 0

##########
#  Main  #
##########

myList = [1,2,4,4,2,4,1]
myList2 = [1,2,2,1]
myList3 = [1,2,1,1,1]
myList4 = [1,1,2,4,4,4,1,1,1,1,2]

print "Solution: ", solution(myList)
print "Solution: ", solution(myList2)
print "Solution: ", solution(myList3)
print "Solution: ", solution(myList4)
